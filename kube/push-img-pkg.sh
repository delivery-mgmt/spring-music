#!/bin/bash -x

version=$current_version
REGISTRY_IMAGE="${EXT_REGISTRY_IMAGE:-$CI_REGISTRY_IMAGE}"
echo ${docker_creds} > ~/.docker/config.json

mkdir -p config/.imgpkg

echo "image: $image_reference" \
| kbld -f - --imgpkg-lock-output config/.imgpkg/images.yml

cat config/.imgpkg/images.yml

imgpkg push \
  -b ${REGISTRY_IMAGE}-imgpkg/${CI_PROJECT_NAME}:${version} \
  -f config

imgpkg copy \
  -b ${REGISTRY_IMAGE}-imgpkg/${CI_PROJECT_NAME}:${version} \
  --to-repo ${REGISTRY_IMAGE}-imgpkg-bundle/${CI_PROJECT_NAME} --cosign-signatures
