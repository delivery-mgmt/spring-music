#!/bin/bash -x

. kube/kubeconfig.sh $build_cluster_context

IMAGE_NAME="$CI_PROJECT_NAME"
REGISTRY_IMAGE="${EXT_REGISTRY_IMAGE:-$CI_REGISTRY_IMAGE}"
BUILDER="${TBS_BUILDER:-default}"

kp image save "$IMAGE_NAME" \
--tag "$REGISTRY_IMAGE/$IMAGE_NAME" \
--git "${CI_PROJECT_URL}.git" \
--git-revision "$CI_COMMIT_SHA" \
--cluster-builder $BUILDER -w

latest_image=$(kubectl get image -n default $IMAGE_NAME -o json | jq .status.latestImage)
echo "image_reference=${latest_image}" >> image.env
