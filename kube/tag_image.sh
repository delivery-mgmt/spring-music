#!/bin/sh -x

mkdir -p ~/.docker
echo ${docker_creds} > ~/.docker/config.json
image=$(echo $image_reference| tr -d '"')
crane auth login $harbor_registry -u $harbor_user -p $harbor_password
crane tag $image $CI_PIPELINE_ID
crane tag $image $CI_COMMIT_SHORT_SHA
crane tag $image ${current_version}

