#!/bin/bash -x

echo ${docker_creds} > ~/.docker/config.json
image=$(echo $image_reference| tr -d '"')
echo ${cosign_key} | base64 -d > key
COSIGN_PASSWORD=${cosign_password} cosign sign -key key $image
